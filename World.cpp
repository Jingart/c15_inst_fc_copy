#include "World.h"


World::World(void)
{
}


World::~World(void)
{
}


void World::Init() {

	//Effects::InitAll(md3dDevice);
	//InputLayouts::InitAll(md3dDevice);

	//CubeTerr.Init(md3dDevice, md3dImmediateContext);

	//Terrain::InitInfo tii;
	//tii.HeightMapFilename = L"Textures/terrain.raw";
	//tii.LayerMapFilename0 = L"Textures/grass.dds";
	//tii.LayerMapFilename1 = L"Textures/darkdirt.dds";
	//tii.LayerMapFilename2 = L"Textures/stone.dds";
	//tii.LayerMapFilename3 = L"Textures/lightdirt.dds";
	//tii.LayerMapFilename4 = L"Textures/snow.dds";
	//tii.BlendMapFilename = L"Textures/blend.dds";
	//tii.HeightScale = 50.0f;
	//tii.HeightmapWidth = 2049;
	//tii.HeightmapHeight = 2049;
	//tii.CellSpacing = 0.5f;

	//mTerrain.Init(md3dDevice, md3dImmediateContext, tii);

	mTileTerrain.Init(md3dDevice, md3dImmediateContext);

}


void World::Update(){
}


void World::Draw(const Camera& cam, DirectionalLight lights[3]){

	//CubeTerr.Draw(md3dImmediateContext, mCam, mDirLights);
	mTileTerrain.Draw(md3dImmediateContext, cam, lights);
	//mTerrain.Draw(md3dImmediateContext, mCam, mDirLights);

}


void World::Setmd3dDevice(ID3D11Device* d3dDevice){
	md3dDevice = d3dDevice;
}


void World::Setmd3dImmediateContext(ID3D11DeviceContext* d3dImmediateContext){
	md3dImmediateContext = d3dImmediateContext;
}

//void World::SetCamera(Camera& cam){
//	mCam = cam;
//}

//void World::SetLights(DirectionalLight lights[3]){
//	mLights = lights;
//}


