#include "CubeTerrain.h"
#include "Terrain.h"
#include "TileTerrain.h"
#include <list>
#include "WorldObject.h"

#pragma once
class World
{
public:
	World(void);
	~World(void);

	void Init();
	void Update();
	void Draw(const Camera& cam, DirectionalLight lights[3]);

	void Setmd3dDevice(ID3D11Device* md3dDevice);
	void Setmd3dImmediateContext(ID3D11DeviceContext* md3dImmediateContext);
	void SetCamera(Camera& cam);
	//void SetLights(DirectionalLight lights[3]);
	void addWorldObject(WorldObject object);

private:
	CubeTerrain CubeTerr;
	Terrain mTerrain;
	TileTerrain mTileTerrain;

	ID3D11Device* md3dDevice;
	ID3D11DeviceContext* md3dImmediateContext;
	//Camera& mCam;
	//DirectionalLight mLights[3];
	std::list<WorldObject> mWorldObjects;

};

