#pragma once

#include "MathHelper.h"

class WorldObject
{

public:
	WorldObject(void);
	WorldObject(XMFLOAT4X4);
	~WorldObject(void);

	XMFLOAT4X4 getWorldMatrix();
	void setWorldMatrix(XMFLOAT4X4);
	void setWorldMatrix(float, float, float);

private:
	XMFLOAT4X4 mObjectWorld;

};

