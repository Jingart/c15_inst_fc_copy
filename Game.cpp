//***************************************************************************************
// InstancingAndCullingDemo.cpp by Frank Luna (C) 2011 All Rights Reserved.
//
// Demonstrates hardware instancing and frustum culling.  Only objects intersecting
// the frustum are instanced and drawn.
//
// Controls:
//		Hold the left mouse button down and move the mouse to rotate.
//      Hold the right mouse button down to zoom in and out.
//      '1' Turn on frustum culling.
//      '2' Turn off frustum culling.
//
//***************************************************************************************

#include "d3dApp.h"
#include "d3dx11Effect.h"
//#include "GeometryGenerator.h"
//#include "MathHelper.h"
#include "LightHelper.h"
#include "Effects.h"
#include "Vertex.h"
#include "Camera.h"

//#include "CubeTerrain.h"
//#include "Terrain.h"
//#include "TileTerrain.h"
#include "World.h"

#include "xnacollision.h"

struct InstancedData
{
	XMFLOAT4X4 World;
	XMFLOAT4 Color;
};

class InstancingAndCullingApp : public D3DApp 
{
public:
	InstancingAndCullingApp(HINSTANCE hInstance);
	~InstancingAndCullingApp();

	bool Init();
	void OnResize();
	void UpdateScene(float dt);
	void DrawScene(); 

	void OnMouseDown(WPARAM btnState, int x, int y);
	void OnMouseUp(WPARAM btnState, int x, int y);
	void OnMouseMove(WPARAM btnState, int x, int y);

private:
	void BuildSkullGeometryBuffers();
	void BuildInstancedBuffer();

private:
	XNA::Frustum mCamFrustum;
	DirectionalLight mDirLights[3];
	Camera mCam;

	//CubeTerrain CubeTerr;
	//Terrain mTerrain;
	//TileTerrain mTileTerrain;

	World mWorld;

	POINT mLastMousePos;

	ID3D11RasterizerState* mWireframeRS;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	InstancingAndCullingApp theApp(hInstance);
	
	if( !theApp.Init() )
		return 0;
	
	return theApp.Run();
}
 

InstancingAndCullingApp::InstancingAndCullingApp(HINSTANCE hInstance)
: D3DApp(hInstance), mWireframeRS(0)
{
	mMainWndCaption = L"TerrainPrototype";
	
	srand((unsigned int)time((time_t *)NULL));

	mLastMousePos.x = 0;
	mLastMousePos.y = 0;

	mCam.SetPosition(0.0f, 5.0f, 0.0f);

	XMMATRIX I = XMMatrixIdentity();

	mDirLights[0].Ambient  = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	mDirLights[0].Diffuse  = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	mDirLights[0].Specular = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	mDirLights[0].Direction = XMFLOAT3(0.57735f, -0.57735f, 0.57735f);

	mDirLights[1].Ambient  = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	mDirLights[1].Diffuse  = XMFLOAT4(0.20f, 0.20f, 0.20f, 1.0f);
	mDirLights[1].Specular = XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f);
	mDirLights[1].Direction = XMFLOAT3(-0.57735f, -0.57735f, 0.57735f);

	mDirLights[2].Ambient  = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	mDirLights[2].Diffuse  = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	mDirLights[2].Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	mDirLights[2].Direction = XMFLOAT3(0.0f, -0.707f, -0.707f);
}

InstancingAndCullingApp::~InstancingAndCullingApp()
{
	Effects::DestroyAll();
	InputLayouts::DestroyAll(); 
	ReleaseCOM(mWireframeRS);
}

bool InstancingAndCullingApp::Init()
{
	if(!D3DApp::Init())
		return false;

	D3D11_RASTERIZER_DESC wireframeDesc;
	ZeroMemory(&wireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	wireframeDesc.FillMode = D3D11_FILL_SOLID;
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FrontCounterClockwise = false;
	wireframeDesc.DepthClipEnable = true;
	HR(md3dDevice->CreateRasterizerState(&wireframeDesc, &mWireframeRS));

	// Must init Effects first since InputLayouts depend on shader signatures.
	Effects::InitAll(md3dDevice);
	InputLayouts::InitAll(md3dDevice);

	//CubeTerr.Init(md3dDevice, md3dImmediateContext);

	//Terrain::InitInfo tii;
	//tii.HeightMapFilename = L"Textures/terrain.raw";
	//tii.LayerMapFilename0 = L"Textures/grass.dds";
	//tii.LayerMapFilename1 = L"Textures/darkdirt.dds";
	//tii.LayerMapFilename2 = L"Textures/stone.dds";
	//tii.LayerMapFilename3 = L"Textures/lightdirt.dds";
	//tii.LayerMapFilename4 = L"Textures/snow.dds";
	//tii.BlendMapFilename = L"Textures/blend.dds";
	//tii.HeightScale = 50.0f;
	//tii.HeightmapWidth = 2049;
	//tii.HeightmapHeight = 2049;
	//tii.CellSpacing = 0.5f;

	//mTerrain.Init(md3dDevice, md3dImmediateContext, tii);

	//mTileTerrain.Init(md3dDevice, md3dImmediateContext);
	mWorld.Setmd3dDevice(md3dDevice);
	mWorld.Setmd3dImmediateContext(md3dImmediateContext);
	mWorld.Init();

	return true;
}

void InstancingAndCullingApp::OnResize()
{
	D3DApp::OnResize();

	mCam.SetLens(0.25f*MathHelper::Pi, AspectRatio(), 1.0f, 1000.0f);

	//// Build the frustum from the projection matrix in view space.
	//ComputeFrustumFromProjection(&mCamFrustum, &mCam.Proj());

	//CubeTerr.OnResize(mCam);
}

void InstancingAndCullingApp::UpdateScene(float dt)
{
	//
	// Control the camera.
	//
	if( GetAsyncKeyState('W') & 0x8000 )
		mCam.Walk(30.0f*dt);

	if( GetAsyncKeyState('S') & 0x8000 )
		mCam.Walk(-30.0f*dt);

	if( GetAsyncKeyState('A') & 0x8000 )
		mCam.Strafe(-30.0f*dt);

	if( GetAsyncKeyState('D') & 0x8000 )
		mCam.Strafe(30.0f*dt);
		
	//if( GetAsyncKeyState('1') & 0x8000 )
	//	mFrustumCullingEnabled = true;

	//if( GetAsyncKeyState('2') & 0x8000 )
	//	mFrustumCullingEnabled = false;

	mCam.UpdateViewMatrix();

	mWorld.Update();

	//CubeTerr.Update(md3dImmediateContext, mCam, dt);
}

void InstancingAndCullingApp::DrawScene()
{
	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&Colors::Silver));
	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);
	md3dImmediateContext->RSSetState(mWireframeRS);

	//CubeTerr.Draw(md3dImmediateContext, mCam, mDirLights);
	//mTileTerrain.Draw(md3dImmediateContext, mCam, mDirLights);
	//mTerrain.Draw(md3dImmediateContext, mCam, mDirLights);

	mWorld.Draw(mCam, mDirLights);


	HR(mSwapChain->Present(0, 0));
}

void InstancingAndCullingApp::OnMouseDown(WPARAM btnState, int x, int y)
{
	mLastMousePos.x = x;
	mLastMousePos.y = y;

	SetCapture(mhMainWnd);
}

void InstancingAndCullingApp::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void InstancingAndCullingApp::OnMouseMove(WPARAM btnState, int x, int y)
{
	if( (btnState & MK_LBUTTON) != 0 )
	{
		// Make each pixel correspond to a quarter of a degree.
		float dx = XMConvertToRadians(0.25f*static_cast<float>(x - mLastMousePos.x));
		float dy = XMConvertToRadians(0.25f*static_cast<float>(y - mLastMousePos.y));

		mCam.Pitch(dy);
		mCam.RotateY(dx);
	}

	mLastMousePos.x = x;
	mLastMousePos.y = y;
}