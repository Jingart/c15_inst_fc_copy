#include "WorldObject.h"


WorldObject::WorldObject(void)
{
	XMMATRIX objScale = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX objOffset = XMMatrixTranslation(0.0f, 0.0f, 0.0f);
	XMStoreFloat4x4(&mObjectWorld, XMMatrixMultiply(objScale, objOffset));
}

WorldObject::WorldObject(XMFLOAT4X4 world)
{
	XMMATRIX objScale = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX objOffset = XMMatrixTranslation(0.0f, 0.0f, 0.0f);
	XMStoreFloat4x4(&mObjectWorld, XMMatrixMultiply(objScale, objOffset));
}


WorldObject::~WorldObject(void)
{
}


XMFLOAT4X4 WorldObject::getWorldLocal() {
	return mObjectWorld;
}

void WorldObject::setWorldLocal(float x, float y, float z) {
	XMStoreFloat4x4(&mObjectWorld, XMMatrixTranslation(x, y, z));
}
