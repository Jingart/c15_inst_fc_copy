#pragma once

#include "d3dUtil.h"
#include "Camera.h"

#include "xnacollision.h"

class CubeTerrain
{
public:
	struct InstancedCullingData
	{
		XMFLOAT4X4 World;
		XMFLOAT4 Color;

	};

	CubeTerrain(void);
	~CubeTerrain(void);

	void Init(ID3D11Device* device, ID3D11DeviceContext* dc);
	void Update(ID3D11DeviceContext* dc, const Camera& cam, float dt);
	void Draw(ID3D11DeviceContext* dc, const Camera& cam, DirectionalLight lights[3]);
	void OnResize(const Camera& cam);

	std::vector<float> LoadHeightmap();
	void NoiseHeightmap();
private:
	std::vector<float> mHeightmap;

	ID3D11Buffer* mCubeTerrainVB;
	ID3D11Buffer* mCubeTerrainIB;
	ID3D11Buffer* mInstancedBuffer;

	UINT mVisibleObjectCount;
	UINT mCubeIndexCount;

	XNA::AxisAlignedBox mCubeAABB;

	// Keep a system memory copy of the world matrices for culling.
	std::vector<InstancedCullingData> mInstancedCullingData;

	// Define transformations from local spaces to world space.
	XMFLOAT4X4 mCubeTerrainWorld;

	Material mCubeMat;

	XNA::Frustum mCamFrustum;

	void BuildInstancedBuffer(ID3D11Device* device);
	void BuildCubeGeometryBuffers(ID3D11Device* device);
	


};

