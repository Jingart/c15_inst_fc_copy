#pragma once

#include "d3dUtil.h"
#include "Camera.h"
#include "GeometryGenerator.h"
#include "xnacollision.h"

namespace Vertex
{
	struct PosNormal
	{
		XMFLOAT3 Pos;
		XMFLOAT3 Normal;
	};
}

class TileTerrain
{
public:

	TileTerrain(void);
	~TileTerrain(void);

	void Init(ID3D11Device* device, ID3D11DeviceContext* dc);
	void Update(ID3D11DeviceContext* dc, const Camera& cam, float dt);
	void Draw(ID3D11DeviceContext* dc, const Camera& cam, DirectionalLight lights[3]);
	void OnResize(const Camera& cam);

private:
	void BuildShapeGeometryBuffers(ID3D11Device* device);
	void BuildSkullGeometryBuffers(ID3D11Device* device);
	GeometryGenerator::MeshData GenerateNormals(GeometryGenerator::MeshData obj);
	std::vector<float> LoadHeightmap();

private:
	std::vector<float> mHeightmap;

	Material mGridMat;
	XMFLOAT4X4 mGridWorld;
	UINT mGridIndexCount;

	ID3D11Buffer* mShapesVB;
	ID3D11Buffer* mShapesIB;

	UINT mLightCount;	
};

