#include "TileTerrain.h"

#include "CubeTerrain.h"
#include "GeometryGenerator.h"
#include "Vertex.h"
#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "MathHelper.h"
#include "LightHelper.h"
#include "Effects.h"



TileTerrain::TileTerrain(void)
:mShapesVB(0), mShapesIB(0), mLightCount(0)
{

	//WorldObject gridWorld;
	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&mGridWorld, I);

	mGridMat.Ambient  = XMFLOAT4(0.48f, 0.77f, 0.46f, 1.0f);
	mGridMat.Diffuse  = XMFLOAT4(0.48f, 0.77f, 0.46f, 1.0f);
	mGridMat.Specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 16.0f);
}


TileTerrain::~TileTerrain(void)
{
	ReleaseCOM(mShapesVB);
	ReleaseCOM(mShapesIB);
}


void TileTerrain::Init(ID3D11Device* device, ID3D11DeviceContext* dc) {

	BuildShapeGeometryBuffers(device);
}


void TileTerrain::Update(ID3D11DeviceContext* dc, const Camera& cam, float dt){
}


void TileTerrain::Draw(ID3D11DeviceContext* dc, const Camera& cam, DirectionalLight lights[3]){


	dc->IASetInputLayout(InputLayouts::TileTerrain);
    dc->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
 
	UINT stride = sizeof(Vertex::TileTerrain);
    UINT offset = 0;

	XMMATRIX view     = cam.View();
	XMMATRIX proj     = cam.Proj();
	XMMATRIX viewProj = cam.ViewProj();

	// Set per frame constants.
	Effects::TileTerrainEffectFX->SetDirLights(lights);
	Effects::TileTerrainEffectFX->SetEyePosW(cam.GetPosition());

	ID3DX11EffectTechnique* activeTech = Effects::TileTerrainEffectFX->Light1Tech;

    D3DX11_TECHNIQUE_DESC techDesc;
    activeTech->GetDesc( &techDesc );
    for(UINT p = 0; p < techDesc.Passes; ++p)
    {
		dc->IASetVertexBuffers(0, 1, &mShapesVB, &stride, &offset);
		dc->IASetIndexBuffer(mShapesIB, DXGI_FORMAT_R32_UINT, 0);

		// Draw the grid.
		XMMATRIX world = XMLoadFloat4x4(&mGridWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world*view*proj;

		Effects::TileTerrainEffectFX->SetWorld(world);
		Effects::TileTerrainEffectFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::TileTerrainEffectFX->SetWorldViewProj(worldViewProj);
		Effects::TileTerrainEffectFX->SetMaterial(mGridMat);

		activeTech->GetPassByIndex(p)->Apply(0, dc);
		dc->DrawIndexed(mGridIndexCount, 0, 0);
    }
}


std::vector<float> TileTerrain::LoadHeightmap()
{
	UINT height = 257;
	UINT width = 257;
	UINT scale = 10.0f;
	std::vector<float> heightmap;

	// A height for each vertex
	std::vector<unsigned char> in( height * width );

	// Open the file.
	std::ifstream inFile;
	inFile.open(L"Textures/terrain9.raw", std::ios_base::binary);

	if(inFile)
	{
		// Read the RAW bytes.
		inFile.read((char*)&in[0], (std::streamsize)in.size());

		// Done with file.
		inFile.close();
	}

	// Copy the array data into a float array and scale it.
	
	heightmap.resize(height * width, 0);
	for(UINT i = 0; i < height * width; ++i)
	{
		heightmap[i] = (in[i] / 255.0f)*scale;
		heightmap[i] = floor(heightmap[i] + 0.5f);
		//mHeightmap[i] = in[i];
	}

	return heightmap;
}


GeometryGenerator::MeshData TileTerrain::GenerateNormals(GeometryGenerator::MeshData obj) {

	for (int i = 0; i < obj.Vertices.size(); i++)
	{
	  XMFLOAT3 fl(0.0f, 0.0f, 0.0f);
      obj.Vertices[i].Normal = fl;
	}

	//build normals
	for (int i = 0; i < obj.Indices.size() / 3; i++)
	{
		XMVECTOR p1 = XMLoadFloat3(&obj.Vertices[obj.Indices[i*3+2]].Position);
		XMVECTOR p2 = XMLoadFloat3(&obj.Vertices[obj.Indices[i*3]].Position);
		XMVECTOR p3 = XMLoadFloat3(&obj.Vertices[obj.Indices[i*3+1]].Position);

		XMVECTOR E1 = XMVectorSubtract(p2, p3);
		XMVECTOR E2 = XMVectorSubtract(p3, p1);

		XMVECTOR nor = XMVector3Cross(E1, E2);

		nor = XMVector3Normalize(nor);

		XMVECTOR vb1 = XMLoadFloat3(&obj.Vertices[obj.Indices[i * 3]].Normal);
		XMStoreFloat3(&obj.Vertices[obj.Indices[i * 3]].Normal, XMVectorAdd(vb1, nor));

		XMVECTOR vb2 = XMLoadFloat3(&obj.Vertices[obj.Indices[i * 3 + 1]].Normal);
		XMStoreFloat3(&obj.Vertices[obj.Indices[i * 3 + 1]].Normal, XMVectorAdd(vb2, nor));

		XMVECTOR vb3 = XMLoadFloat3(&obj.Vertices[obj.Indices[i * 3 + 2]].Normal);
		XMStoreFloat3(&obj.Vertices[obj.Indices[i * 3 + 2]].Normal, XMVectorAdd(vb3, nor));
	}
	
	for (int i = 0; i < obj.Vertices.size(); i++) {
		XMVECTOR n = XMLoadFloat3(&obj.Vertices[i].Normal);
		n = XMVector3Normalize(n);
		XMStoreFloat3(&obj.Vertices[i].Normal, n);
	}

	return obj;
}

void TileTerrain::BuildShapeGeometryBuffers(ID3D11Device* device)
{
	std::vector<float> heightmap;
	heightmap = LoadHeightmap();

	GeometryGenerator::MeshData grid;
	GeometryGenerator geoGen;

	geoGen.CreateGrid(257.0f, 257.0f, 257, 257, grid);

	for(size_t i = 0; i < grid.Vertices.size(); ++i)
	{
		XMFLOAT3 p = grid.Vertices[i].Position;
		p.y = heightmap[i];
		grid.Vertices[i].Position = p;
	}

	grid = GenerateNormals(grid);

	//mGridVertexOffset = 0;
	mGridIndexCount = grid.Indices.size();
	//mGridIndexOffset = 0;

	std::vector<Vertex::PosNormal> vertices(grid.Vertices.size());
	for(size_t i = 0; i < grid.Vertices.size(); ++i)
	{
		vertices[i].Pos    = grid.Vertices[i].Position;//grid.Vertices[i].Position;
		vertices[i].Normal = grid.Vertices[i].Normal;//grid.Vertices[i].Normal;
	}

    D3D11_BUFFER_DESC vbd;
    vbd.Usage = D3D11_USAGE_IMMUTABLE;
    vbd.ByteWidth = sizeof(Vertex::TileTerrain) * vertices.size();
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = 0;
    vbd.MiscFlags = 0;
    D3D11_SUBRESOURCE_DATA vinitData;
    vinitData.pSysMem = &vertices[0];
    HR(device->CreateBuffer(&vbd, &vinitData, &mShapesVB));

	std::vector<UINT> indices;
	indices.insert(indices.end(), grid.Indices.begin(), grid.Indices.end());

	D3D11_BUFFER_DESC ibd;
    ibd.Usage = D3D11_USAGE_IMMUTABLE;
    ibd.ByteWidth = sizeof(UINT) * indices.size();
    ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ibd.CPUAccessFlags = 0;
    ibd.MiscFlags = 0;
    D3D11_SUBRESOURCE_DATA iinitData;
    iinitData.pSysMem = &indices[0];
    HR(device->CreateBuffer(&ibd, &iinitData, &mShapesIB));
}
