#include "CubeTerrain.h"
#include "GeometryGenerator.h"
#include "Vertex.h"
#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "MathHelper.h"
#include "LightHelper.h"
#include "Effects.h"



CubeTerrain::CubeTerrain(void)
:mCubeTerrainVB(0), mCubeTerrainIB(0), mCubeIndexCount(0), mInstancedBuffer(0),
  mVisibleObjectCount(0)
{
	//XMMATRIX skullScale = XMMatrixScaling(0.5f, 0.5f, 0.5f);
	//XMMATRIX skullOffset = XMMatrixTranslation(0.0f, 1.0f, 0.0f);
	//XMStoreFloat4x4(&mCubeTerrainWorld, XMMatrixMultiply(skullScale, skullOffset));

	mCubeMat.Ambient  = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
	mCubeMat.Diffuse  = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	mCubeMat.Specular = XMFLOAT4(0.8f, 0.8f, 0.8f, 16.0f);
}


CubeTerrain::~CubeTerrain(void)
{
	ReleaseCOM(mCubeTerrainVB);
	ReleaseCOM(mCubeTerrainIB);
	ReleaseCOM(mInstancedBuffer);
}


void CubeTerrain::Init(ID3D11Device* device, ID3D11DeviceContext* dc) {
	
	LoadHeightmap();
	BuildCubeGeometryBuffers(device);
	BuildInstancedBuffer(device);
}


void CubeTerrain::Update(ID3D11DeviceContext* dc, const Camera& cam, float dt){

	mVisibleObjectCount = 0;
	bool mFrustumCullingEnabled = false;

	if(false)
	{
		XMVECTOR detView = XMMatrixDeterminant(cam.View());
		XMMATRIX invView = XMMatrixInverse(&detView, cam.View());
	
		D3D11_MAPPED_SUBRESOURCE mappedData; 
		dc->Map(mInstancedBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData);
		InstancedCullingData* dataView = reinterpret_cast<InstancedCullingData*>(mappedData.pData);


		XMMATRIX world = XMLoadFloat4x4(&mCubeTerrainWorld);
		XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(world), world);

		// View space to the world space.
		XMMATRIX toWorld = XMMatrixMultiply(invView, invWorld);

		// Decompose the matrix into its individual parts.
		XMVECTOR scale;
		XMVECTOR rotQuat;
		XMVECTOR translation;
		XMMatrixDecompose(&scale, &rotQuat, &translation, world);

		// Transform the camera frustum from view space to the object's local space.
		XNA::Frustum worldspaceFrustum;
		XNA::TransformFrustum(&worldspaceFrustum, &mCamFrustum, XMVectorGetX(scale), rotQuat, translation);


		for(UINT i = 0; i < mInstancedCullingData.size(); ++i)
		{
			//XMMATRIX W = XMLoadFloat4x4(&mInstancedCullingData[i].World);
			//XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);

			//// View space to the object's local space.
			//XMMATRIX toLocal = XMMatrixMultiply(invView, invWorld);
		
			//// Decompose the matrix into its individual parts.
			//XMVECTOR scale;
			//XMVECTOR rotQuat;
			//XMVECTOR translation;
			//XMMatrixDecompose(&scale, &rotQuat, &translation, toLocal);

			//// Transform the camera frustum from view space to the object's local space.
			//XNA::Frustum localspaceFrustum;
			//XNA::TransformFrustum(&localspaceFrustum, &mCamFrustum, XMVectorGetX(scale), rotQuat, translation);

			// Perform the box/frustum intersection test in local space.


			/*if(XNA::IntersectAxisAlignedBoxFrustum(&mCubeAABB, &localspaceFrustum) != 0)*/
			if(XNA::IntersectAxisAlignedBoxFrustum(&mCubeAABB, &worldspaceFrustum) != 0)
			{
				// Write the instance data to dynamic VB of the visible objects.
				dataView[mVisibleObjectCount++] = mInstancedCullingData[i];
			}
		}

		dc->Unmap(mInstancedBuffer, 0);
	}
	else // No culling enabled, draw all objects.
	{
		D3D11_MAPPED_SUBRESOURCE mappedData; 
		dc->Map(mInstancedBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData);

		InstancedCullingData* dataView = reinterpret_cast<InstancedCullingData*>(mappedData.pData);

		for(UINT i = 0; i < mInstancedCullingData.size(); ++i)
		{
			dataView[mVisibleObjectCount++] = mInstancedCullingData[i];
		}

		dc->Unmap(mInstancedBuffer, 0);
	}

	/*
	std::wostringstream outs;   
	outs.precision(6);
	outs << L"Instancing and Culling Demo" << 
		L"    " << mVisibleObjectCount << 
		L" objects visible out of " << mInstancedCullingData.size();
	mMainWndCaption = outs.str();
	*/

}


void CubeTerrain::Draw(ID3D11DeviceContext* dc, const Camera& cam, DirectionalLight lights[3])
{

	dc->IASetInputLayout(InputLayouts::InstancedBasic32);
    dc->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
 
	UINT stride[2] = {sizeof(Vertex::Basic32), sizeof(InstancedCullingData)};
    UINT offset[2] = {0,0};

	ID3D11Buffer* vbs[2] = {mCubeTerrainVB, mInstancedBuffer};
 
	//XMMATRIX view     = cam.View();
	//XMMATRIX proj     = cam.Proj();
	XMMATRIX viewProj = cam.ViewProj();

	// Set per frame constants.
	Effects::InstancedBasicFX->SetDirLights(lights);
	Effects::InstancedBasicFX->SetEyePosW(cam.GetPosition());
 
	ID3DX11EffectTechnique* activeTech = Effects::InstancedBasicFX->Light3Tech;
	 
    D3DX11_TECHNIQUE_DESC techDesc;
    activeTech->GetDesc( &techDesc );
	for(UINT p = 0; p < techDesc.Passes; ++p)
    {
		// Draw the skull.
		dc->IASetVertexBuffers(0, 2, vbs, stride, offset);
		dc->IASetIndexBuffer(mCubeTerrainIB, DXGI_FORMAT_R32_UINT, 0);

		XMMATRIX world = XMLoadFloat4x4(&mCubeTerrainWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		Effects::InstancedBasicFX->SetWorld(world);
		Effects::InstancedBasicFX->SetWorldInvTranspose(world);
		Effects::InstancedBasicFX->SetViewProj(viewProj);
		Effects::InstancedBasicFX->SetMaterial(mCubeMat);

		activeTech->GetPassByIndex(p)->Apply(0, dc);
		dc->DrawIndexedInstanced(mCubeIndexCount, mVisibleObjectCount, 0, 0, 0);
	}
	
	//HR(mSwapChain->Present(0, 0));
}


void CubeTerrain::OnResize(const Camera& cam)
{
	// Build the frustum from the projection matrix in view space.
	ComputeFrustumFromProjection(&mCamFrustum, &cam.Proj());
}


void CubeTerrain::BuildCubeGeometryBuffers(ID3D11Device* device)
{
	
	GeometryGenerator::MeshData box;
	GeometryGenerator geoGen;
	geoGen.CreateBox(1.0f, 1.0f, 1.0f, box);


		// Cache the vertex offsets to each object in the concatenated vertex buffer.
	//mBoxVertexOffset      = 0;

	// Cache the index count of each object.
	mCubeIndexCount = box.Indices.size();

	// Cache the starting index for each object in the concatenated index buffer.
	//mBoxIndexOffset      = 0;
	
	//UINT totalVertexCount = box.Vertices.size();

	//UINT totalIndexCount = mBoxIndexCount;

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	std::vector<Vertex::Basic32> vertices(box.Vertices.size());

	UINT k = 0;
	for(size_t i = 0; i < box.Vertices.size(); ++i, ++k)
	{
		vertices[k].Pos    = box.Vertices[i].Position;
		vertices[k].Normal = box.Vertices[i].Normal;
		vertices[k].Tex    = box.Vertices[i].TexC;
	}



	//AABB box
	XMFLOAT3 vMinf3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 vMaxf3(0.0f, 0.0f, 0.0f);
	
	XMVECTOR vMin = XMLoadFloat3(&vMinf3);
	XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

	XMStoreFloat3(&mCubeAABB.Center, vMin);
	XMStoreFloat3(&mCubeAABB.Extents, vMax);




    D3D11_BUFFER_DESC vbd;
    vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex::Basic32) * box.Vertices.size();
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = 0;
    vbd.MiscFlags = 0;
    D3D11_SUBRESOURCE_DATA vinitData;
    vinitData.pSysMem = &vertices[0];
    HR(device->CreateBuffer(&vbd, &vinitData, &mCubeTerrainVB));

	//
	// Pack the indices of all the meshes into one index buffer.
	//
	std::vector<UINT> indices;
	indices.insert(indices.end(), box.Indices.begin(), box.Indices.end());

	D3D11_BUFFER_DESC ibd;
    ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT) * box.Indices.size();
    ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ibd.CPUAccessFlags = 0;
    ibd.MiscFlags = 0;
    D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = &indices[0];
    HR(device->CreateBuffer(&ibd, &iinitData, &mCubeTerrainIB));
}

void CubeTerrain::BuildInstancedBuffer(ID3D11Device* device)
{
	std::vector<float> hMap;
	LoadHeightmap();

	const int n = 129;
	mInstancedCullingData.resize(n*n);
	int count = 0;

	for(int k = 0; k < n; ++k)
	{

		for(int i = 0; i < n; ++i)
		{
			mInstancedCullingData[count].World = XMFLOAT4X4(
											1.0f, 0.0f, 0.0f, 0.0f,
											0.0f, 1.0f, 0.0f, 0.0f,
											0.0f, 0.0f, 1.0f, 0.0f,
											//k, MathHelper::RandF(0.0f, 1.0f), i + 1.5f, 1.0f);
											k, mHeightmap[count], i + 1.5f, 1.0f);

			// Random color.
			//mInstancedCullingData[count].Color.x = MathHelper::RandF(0.0f, 1.0f);
			//mInstancedCullingData[count].Color.y = MathHelper::RandF(0.0f, 1.0f);
			//mInstancedCullingData[count].Color.z = MathHelper::RandF(0.0f, 1.0f);
			//mInstancedCullingData[count].Color.w = 1.0f;
			mInstancedCullingData[count].Color.x = 0.1f;
			mInstancedCullingData[count].Color.y = 0.9f;
			mInstancedCullingData[count].Color.z = 0.2f;
			mInstancedCullingData[count].Color.w = 1.0f;


			count++;
		}
	}


	D3D11_BUFFER_DESC vbd;
    vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.ByteWidth = sizeof(InstancedCullingData) * mInstancedCullingData.size();
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
	
    HR(device->CreateBuffer(&vbd, 0, &mInstancedBuffer));
}


std::vector<float> CubeTerrain::LoadHeightmap()
{
	UINT height = 129;
	UINT width = 129;
	UINT scale = 10.0f;
	// A height for each vertex
	std::vector<unsigned char> in( height * width );

	// Open the file.
	std::ifstream inFile;
	inFile.open(L"Textures/terrain8.raw", std::ios_base::binary);

	if(inFile)
	{
		// Read the RAW bytes.
		inFile.read((char*)&in[0], (std::streamsize)in.size());

		// Done with file.
		inFile.close();
	}

	// Copy the array data into a float array and scale it.
	
	mHeightmap.resize(height * width, 0);
	for(UINT i = 0; i < height * width; ++i)
	{
		mHeightmap[i] = (in[i] / 255.0f)*scale;
		mHeightmap[i] = floor(mHeightmap[i] + 0.5f);
		//mHeightmap[i] = in[i];
	}

	return mHeightmap;
}


void CubeTerrain::NoiseHeightmap()
{


}